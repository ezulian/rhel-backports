#!/bin/bash
set -euo pipefail


CURLRC_SRC=https://gitlab.com/cki-project/containers/-/raw/main/files/curlrc
CURLRC_FILE=/tmp/curlrc

# Install packages using
# Args:
#   $1: Package name exectuable
#   $[2:]: List of packages to install
function install_packages {
    package_manager=$1
    shift
    "${package_manager}" -y install "${@}"
}

# Download curl config
function download_curl_config {
    curl --location --silent --fail --show-error "${CURLRC_SRC}" --output "${CURLRC_FILE}"

}
# Download and uncompress a tgz file
# Args:
#   $1: file URI
#   $2: Folder to uncompress
function download_and_uncompress_tgz {
    if [[ ! -f "${CURLRC_FILE}" ]]; then
        download_curl_config
    fi
    curl --config "${CURLRC_FILE}" "${1}" | tar --extract --gzip --file - --directory "${2}"
}

# Download and uncompress python
# Args:
#   $1: Python version
#   $2: Folder to uncompress
function download_and_uncompress_python {
    url="https://www.python.org/ftp/python/${1}/Python-${1}.tgz"
    dest_dir="${2}"
    download_and_uncompress_tgz "${url}" "${dest_dir}"
}

# Download and uncompress bash
# Args:
#   $1: Bash version
#   $2: Folder to uncompress
function download_and_uncompress_bash {
    url="https://ftpmirror.gnu.org/bash/${1}.tar.gz"
    dest_dir="${2}"
    download_and_uncompress_tgz "${url}" "${dest_dir}"
}

# Download and uncompress jq
# Args:
#   $1: jq version
#   $2: Folder to uncompress
function download_and_uncompress_jq {
    url="https://github.com/jqlang/jq/releases/download/jq-$1/jq-$1.tar.gz"
    dest_dir="${2}"
    download_and_uncompress_tgz "${url}" "${dest_dir}"
}


# Compress and store artifacts
# Args:
#   $1: artifact name
#   $2: source folder
function store_artifact {
    # shellcheck disable=SC2154
    tar -zcf "${CI_PROJECT_DIR}/${1}.tgz" -C "${2}/" .
}

# Configure and install
# Args:
# $@ parameters to configure
function configure_and_install {
    ./configure "${@}"
    make
    make install
}

# Get the "normalized string" to get the right sqlite version
# More info in https://www.sqlite.org/download.html
# Args:
#   $1: the sqlite version using dots
function sqlite_normalized_version {
    IFS='.' read -ra data <<< "${1}"
    local major=${data[0]}
    local minor=${data[1]}
    local patch=${data[2]}
    printf '%01d%02d%02d00' "${major}" "${minor}" "${patch}"
}
