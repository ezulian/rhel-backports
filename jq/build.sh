#!/bin/bash
set -euo pipefail

# shellcheck source-path=SCRIPTDIR
source common_functions.sh

RPM_PACKAGE_MANAGER="yum"
RPM_PACKAGES=(
    diffutils
    gcc
    make
    tar
)
SOURCES_DIR="/usr/src"
ARTIFACT_NAME="jq"
ARTIFACT_SRC="/usr/local"

install_packages "${RPM_PACKAGE_MANAGER}" "${RPM_PACKAGES[@]}"

# shellcheck disable=SC2154
download_and_uncompress_jq "${JQ_VERSION}" "${SOURCES_DIR}"
# shellcheck disable=SC2154
cd "${SOURCES_DIR}/jq-${JQ_VERSION}/"
configure_and_install ""
store_artifact "${ARTIFACT_NAME}" "${ARTIFACT_SRC}"
