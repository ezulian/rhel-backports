# Backports for RHEL

This repo contains the scripts and CI jobs for building newer versions of
different pieces of software for **RHEL**, to be used by CKI [RHEL builder
container images][containers].

The production artifacts can be downloaded from S3 via something like

```bash
 curl -O https://arr-cki-prod-lookaside.s3.us-east-1.amazonaws.com/lookaside/backports/rhel8/arm64/python.tgz
```

## Software versions

| Software | Version | RHEL versions |
|----------|---------|---------------|
| Bash     | 4.4     | RHEL 6/7      |
| jq       | 1.7     | RHEL 6/7/8    |
| OpenSSL  | 1.0.2u  | RHEL 6        |
| Python   | 3.9.13  | RHEL 6/7/8    |
| SQLite   | 3.38.5  | RHEL 6        |

## Supported architectures

| RHEL version | Architectures                |
|--------------|------------------------------|
| RHEL 6       | amd64                        |
| RHEL 7       | amd64                        |
| RHEL 8       | amd64, arm64, ppc64le, s390x |

[containers]: https://gitlab.com/cki-project/containers/
[download artifacts]: https://docs.gitlab.com/ee/api/job_artifacts.html#download-the-artifacts-archive
